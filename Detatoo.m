function out = Detatoo(in)
    msg = bitand(in, 1);
    out = 255*uint8(msg);
end