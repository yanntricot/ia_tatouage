function out = InsertIntoPicture(original, insert)
    original = bitand(original, 254);
    out = bitor(uint8(original), uint8(insert));
end